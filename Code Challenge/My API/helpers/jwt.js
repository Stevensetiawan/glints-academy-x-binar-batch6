const jwt = require('jsonwebtoken');

exports.jwtSign = (data) => {
    console.log("ini obj data di jwt", { data })
    return jwt.sign({ data }, process.env.SECRET);
}

exports.jwtVerify = (token) => {
    console.log("masuk verify", token)
    return jwt.verify(token, process.env.SECRET);
}
