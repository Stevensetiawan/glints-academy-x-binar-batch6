const { Post } = require('../models')
module.exports = (req, res, next) => {

    let id = {
        where: {
            id: req.params.id
        }
    }
    Post.findOne(id)
        .then(post => {
            if (post) {
                if (post.user_id === req.payload.data.id || req.payload.data.role === 'admin') {
                    next()
                } else {
                    res.status(401).json({ msg: "unauthorized" })
                }
            } else {
                res.status(404).json({ msg: "data is not found" })
            }
        }).catch(err => {
            res.status(500).json(err)
        })

}