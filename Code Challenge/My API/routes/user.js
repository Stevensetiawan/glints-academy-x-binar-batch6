const router = require('express').Router()
const user = require('../controllers/user')
const success = require('../middlewares/success')

router.post('/register',user.register,success)
router.post('/login',user.login,success)

module.exports=router