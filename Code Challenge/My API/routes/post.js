const router = require('express').Router()
const post = require('../controllers/post')
const  authorization = require('../middlewares/authorization')
const authentication = require('../middlewares/authentication')
const success = require('../middlewares/success')

router.use(authentication)
router.get('/findAll',post.findAll,success)
router.post('/create',post.create,success)
router.put('/update/:id',authorization,post.update,success)
router.delete('/delete/:id',authorization,post.delete,success)

module.exports=router