const { Post } = require('../models')

exports.create = async function (req, res, next) {
    try {
        const post = await Post.create({
            title: req.body.title,
            content: req.body.content,
            user_id: req.payload.data.id,
            approved: false
        });
        res.status(200);
        res.data = { post };
        next();
    }

    catch (err) {
        res.status(422);
        next(err);
    };
}

exports.findAll = async function (req, res, next) {
    try {
        console.log(req.payload.data,"ini payloadnya")
        if (req.payload.data.role === 'member') {
            const posts = await Post.findAll({
                where: {
                    approved: true
                }
            })
            res.status(200);
            res.data = { posts };
            next()
        } else if (req.payload.data.role === 'admin') {
            const posts = await Post.findAll({
            })
            res.status(200);
            res.data = { posts };
            next()
        }

    }

    catch (err) {
        res.status(404);
        next(err);
    }
}

exports.update = async function (req, res, next) {

    try {
        if (req.payload.data.role === 'admin') {
            let approved = {
                where: {
                    approved: req.body.approved
                }
            }
            await Post.update(approved, {
                where: { id: req.params.id }
            });
            res.status(200);
            res.data = "Successfully updated!";
            next();
        } else if (req.payload.data.role === 'member') {

            let { title, content } = req.body

            let dataUpdate = {
                where : {
                    title,
                    content
                }
            }
            
            await Post.update(dataUpdate, {
                where: { id: req.params.id }
            });
            res.status(200);
            res.data = "Successfully updated!";
            next();
        }

    } catch (err) {
        res.status(422);
        next(err);
    };
}

exports.delete = async function (req, res, next) {
    try {
        await Post.destroy({
            where: { id: req.params.id }
        });
        res.status(200);
        res.data = "Successfully updated!";
        next();
    } catch (err) {
        res.status(500);
        next(err);
    };
}
