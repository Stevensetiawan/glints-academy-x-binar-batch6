const { User } = require('../models')
const bcrypt = require('../helpers/bcrypt')
const jwt = require('../helpers/jwt')

module.exports = {
    async register(req, res, next) {
        const { email, password } = req.body
        try {
            let user = await User.create({
                email,
                password,
                role:"member"
            })
            res.data = user;
            next()

        } catch (err) {
            res.status(422)
            next(err)
        }
    },
    async login(req, res, next) {
        console.log(req.body)
        let { email, password } = req.body
        try {
            let user = await User.findOne({
                where: {
                    email
                }
            })
            if (user) {
                let isValidate = bcrypt.checker(password, user.password)
                if (isValidate) {
                    if (user.role === 'admin') {
                        let tokenAdmin = jwt.jwtSign({
                            id: user.id,
                            email: user.email,
                            role: user.role
                        })
                        res.status(201);
                        res.data = { tokenAdmin }
                        next()
                    } else if (user.role === "member") {
                        let tokenCustomer = jwt.jwtSign({
                            id: user.id,
                            email: user.email,
                            role: user.role
                        })
                        res.status(201);
                        res.data = { tokenCustomer }
                        next()
                    }
                }
            } else {
                res.status(404).json({ msg: "id or email is not found" })
            }

        } catch (err) {
            res.status(401);
            next(err);
        }


    }
}