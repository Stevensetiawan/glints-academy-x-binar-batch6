require('dotenv').config()
const express = require('express')
const morgan = require('morgan');
const app = express()
const port = process.env.PORT || 3000
const router = require('./routes');
const exception = require('./middlewares/exception')

app.get('/', (req, res) => {
    res.status(200).json({
        status: 'success',
        message: "Testing"
    })
})

app.use(express.json());
app.use(morgan('dev'));
app.use('/myAPI/v1', router);

// Apply Exception Handler
exception.forEach(handler =>
  app.use(handler)
);



app.listen(port, () =>
    console.log(`Example app listening at http://localhost:${port}`)
)