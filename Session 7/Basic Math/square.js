function area(s) {
  return s * s;
}

function round(s) {
  return 4 * s;
}

// Export function that will be used in another file
module.exports = {
  area,
  round
};

// Unary