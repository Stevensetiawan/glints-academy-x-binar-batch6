// Switch Case Statement

const answer = 2;

if (answer == 1) {
  console.log("Hello from if");
}

else if (answer == 2) {
  // Do Something
}

else if (answer == 3)  {

}

function handleAnswer() {
  switch (answer) {
    case 1:
      console.log("Hello from switch");
      break;
  
    case 2:
      console.log("Jawaban dua");
      break;
  
    case 3:
      console.log("Jawaban tiga");
      break;
  
    default:
      // Option is not available
      console.log("Jawaban gaada");
  }
}

handleAnswer()

// Or use return
switch (answer) {
  case 1:
    return console.log("Hello from switch");

  case 2:
    return console.log("Jawaban dua");

  case 3:
    return console.log("Jawaban tiga");

  default:
    // Option is not available
    return console.log("Jawaban gaada");
}