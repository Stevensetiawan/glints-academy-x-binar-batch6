const readline = require('readline'); // readline module
const fs = require('fs'); // fs module
const path = require('path'); // path module

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

rl.question("What is you email? ", email => {
  
  rl.question("What is your password? ", password => {
    register(email, password);
    
    rl.close();
  })
})

// __dirname itu buat apa ya?
// __dirname is consistent

function register(email, password) {
  fs.writeFileSync(
    path.resolve(__dirname, '.', 'data.json'), // path module
    JSON.stringify({
      email, password
    }, null, 2) // Change data into string, with JSON format.
  );
}

rl.on('close', () => {
  process.exit
})