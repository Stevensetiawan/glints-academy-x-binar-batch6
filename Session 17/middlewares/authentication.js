const jwt = require('../helpers/jwt')
const { users } = require('../models')

module.exports = (req, res, next) => {
    let access_token  = req.headers.token

    console.log(req.headers, "ini headers")
    
    let payload = null
    // console.log(tokenCustomer,"ini token customer")

    try {
        payload = jwt.jwtVerify(access_token)
    } catch (err) {
        res.status(500).json({
            status: "error",
            errors: [err.message]
        })
    }

    console.log("ini payload", payload)

    // let id = {
    //     where: {
    //         id: payload.data.id
    //     }
    // }

    users.findByPk(payload.data.id)
        .then(user => {
            if (user) {
                req.payloadUser = payload
                next()
            } else {
                res.status(404).json({ message: "data is not found" })
            }
        }).catch(err => {
            res.status(500).json({ message: "internal server error" })
        })
}