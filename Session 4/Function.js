/*
 * f(s) = s * s
 * f(2) = 2 * 2
 * y = f(2)
 *
 * y = 4
 * */

/* This is a real function */
function f(s) {
  return s * s;
}
const y = f(2);

/* Procedure */
function greet() {
  console.log("Hi, how are you?");
  console.log("Where are you from?");
  console.log("Nice to meet you!");
}

// There are three ways to create a function
function functionName() {
  // Do something
}

const anotherFunction = function() {
  // Do something
}

// Lambda
const arrowFunction = () => {
  // Do Something
}

// Lambda with param
const anotherArrow = x => {
  console.log(x);
}

// Lambda with params
const moreArrow = (x, y) => {
  // Do Something
}

// Arrow Function vs Function Keywords
function keywords() {
  console.log(this); // Global Object
}

const arrow = () => {
  console.log(this);
}

function circleArea(r) {
  return 3.14 * (r**2)
}

/*
 * Variable Scopes
 * And Function Procedure;
 * */

function tubeVolume(r, t) {
  // Local Variable
  let luasLingkaran = circleArea(r); // What variable is this?
  return luasLingkaran * t;
}

let result = tubeVolume(4, 5); // Global Variable
function more() {
  console.log(result);
}

function ant() {
  /*
   * What is Math.floor?
   * Find it on Google!
   * Keyword: How to create a random number in Javascript
   * */
  let bulat = Math.floor(result);
  console.log(bulat);
}

// Factory Function -> Deprecated
function Person(name, address) {
  this.name = name; // Instance Variable
  this.address = address;
} 

// It is necessary to use function keyword, otherwise we can't call this
Person.prototype.greet = function() {
  console.log(`Hi, my name is ${this.name}`);
}

const Fikri = new Person("Fikri", "Solo");
Fikri.greet();
