const doesItRain = false;

if (doesItRain) {
  console.log("Use umbrella!");
}

let a = 2; // Will return Boolean DataTypes

if (a == 1) {
  // Do something
} else {
  // Do something
}

let s = "Hello World";
let x = 4;

/*
 * Check data type of a variable or data
 * typeof data
 * */

// If s is a number, then run this code
if (typeof s == "number") {
  console.log(s + 10);
}

function example(x) {
  if (x >= 10) {
    return x / 2;
  } else {
    throw new Error("Input should have been more than 10");
  };
};

try {
  let result = example(9);
  console.log(result);
}

catch(err) {
  console.log(err.message);
}
