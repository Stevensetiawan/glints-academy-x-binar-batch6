Object.defineProperty(Array.prototype, 'sample', {
  get: function() {
    return this[
      Math.floor(
        Math.random() * this.length
      )
    ]
  }
})

// Human is Super Class
class Human {

  constructor(props) {
    let { name, address, lang } = props;
    this.name = name;
    this.address = address;
    this.lang = lang;
  }

  static isLivingInEarth = true;

  introduce() {
    console.log(`Hi, my name is ${this.name}`);
  }

  work() {
    console.log("Working...")
  }

}

class Programmer extends Human {

  constructor(props) {
    super(props);
    // console.log(super.constructor.isLivingInEarth);

    this.progammingLangs = props.progammingLangs || [];
  }

  code() {
    console.log(`Writing some ${this.progammingLangs.sample}`)
  }

  // Override Method
  work() {
    this.code();

    // Super is just this, but it is calling the super class
    super.work();
  }
}

const Ian = new Programmer({
  name: "Ian",
  address: "Malang",
  lang: "Jawa",
  progammingLangs: ["Javascript", "Go"]
})

Ian.work();