#!/usr/bin/env node

const fs = require('fs')
const path = require('path');

// Modify Array Instance Getter
Object.defineProperty(Array.prototype, 'sample', {
  get: function() {
    return this[
      Math.floor(
        Math.random() * this.length
      )
    ]
  }
})

// Modify Array Instance Getter
Object.defineProperty(Array.prototype, 'first', {
  get: function() {
    return this[0]
  }
})

// Modify Array Instance Getter
Object.defineProperty(Array.prototype, 'second', {
  get: function() {
    return this[1]
  }
})

// Modify Array Instance Getter
Object.defineProperty(Array.prototype, 'third', {
  get: function() {
    return this[2]
  }
})


// Modify Array Instance Getter
Object.defineProperty(Array.prototype, 'last', {
  get: function() {
    return this[this.length - 1]
  }
})

let fileBuffer = fs.readFileSync(
  path.join(
    process.cwd(),
    process.argv[2]
  )
)

eval(fileBuffer.toString())
