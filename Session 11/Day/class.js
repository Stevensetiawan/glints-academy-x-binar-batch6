/*
 * Class Declaration
 * Method Signature
 * Static and Instance/Prototype
 * Property
 */

/*
let plainObj = {
  name: "Fikri",
  greet: function() {
    console.log(this);
    console.log(this.name);
  }
}

plainObj.greet();
*/

// Class Declaration

// Data called Human will look like.
class Human {

  constructor(name, address, lang) {
    // Programming Languange in General
    this.name = name; // Instance Property
    this.address = address; // Instance Property
    this.lang = lang; // Instance Property
  }

  // Method Signature => Instance
  introduce() {
    console.log(`Hi, my name is ${this.name}`);
  }

  // Call another method inside the class declaration
  #prepare = () => {
    console.log("Waking up");
    console.log("Taking a bath");
    console.log("Use parfume")
    console.log("On the way to the office");
  }

  // Private Method
  #doWork = () => {
    console.log("Arrive at the office");
    console.log("Actually work");
    console.log("Take a break");
    console.log("Work again");
    console.log("Done");
  }

  // Private Method
  #goHome = () => {
    console.log("Go home");
    console.log("Sleep");
  }

  #test = () => {
    console.log(this);
  }

  #testFunc = function() {
    console.log(this);
  }

  work() {    
    this.#prepare();
    this.#doWork();
    this.#goHome();
  }

  // Publicly Accessible
  moreWork() {
    this.#test();
    this.#testFunc();
  }

  accessStatic() {
    console.log(this.constructor.isLivingInEarth);
    console.log(Human.isLivingInEarth);
  }

  tame(pet) {
    this.pets = Array.isArray(this.pets) ? [...this.pets, pet] : [pet];
  }

  static isLivingInEarth = true;
  static isMortal = false;

  static desctruct() {
    console.log(`Human is mortal, and it is ${this.isMortal}`);
  }
}

/*
// Access Static Props form Instance
Fikri.accessStatic();
console.log(Fikri.constructor.isLivingInEarth);

Fikri.introduce();
Fikri.work();
console.log("Before:", Fikri);

Fikri.tame("Cats");
Fikri.tame("Hamster")
console.log("After:", Fikri);
console.log("Fikri's pet:", Fikri.pets)

Fikri.moreWork();
*/
const Fikri = new Human("Fikri", "Solo", "Indonesia");

console.log(Human.isLivingInEarth);
Human.desctruct();

// At new static method on fly!
Human.iniStatic = function() {
  console.log(this);
}

Human.iniStatic();

// At new protoype method on fly!
Human.prototype.greet = function(name) {
  // this.#doWork(); is not accessible
  console.log(`Hi, ${name}, my name is ${this.name}`)
}

Fikri.greet("Ian");