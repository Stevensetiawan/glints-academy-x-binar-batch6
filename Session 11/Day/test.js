/*
  1. Create a class called Human
  2. Create sub class called Chef

  Human can cook();
  Human can introduce();

  Chef cuisines;
  Chef type => Italian, French;
  Chef can promote();
  Chef can cook() but better // Method Override
  Chef will introduce themself as ${type} chef
  */

class Human {

  constructor(props) {
    this.name = props.name;
  }

  introduce() {
    console.log(`Hi, my name is ${this.name}`)
  }

  cook() {
    console.log("Cook egg");
  }

}

class Chef extends Human {

  constructor(props) {
    super(props);
    this.cuisines = props.cuisines;
    this.type = props.type;
  }

  introduce() {
    super.introduce();
    console.log(`And I'm a ${this.type} chef!`);
  }

  cook() {
    super.cook();
    console.log(`Cook ${this.cuisines.sample}`)
  }

  promote() {
    console.log("I can cook these dishes for you", this.cuisines);
  }

}