Object.defineProperty(Array.prototype, 'sample', {
  get: function() {
    return this[
      Math.floor(
        Math.random() * this.length
      )
    ]
  }
})
