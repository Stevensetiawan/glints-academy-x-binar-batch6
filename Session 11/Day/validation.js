Array.prototype.sample = function() {
  return this[
    Math.floor(
      Math.random() * this.length
    )
  ]
}

// Human is Super Class
class Human {

  constructor(props) {
    this.name = props.name;
    this.address = props.address;
    this.lang = props.lang;
  }

  static isLivingInEarth = true;

  introduce() {
    console.log(`Hi, my name is ${this.name}`);
  }

  work() {
    console.log("Working...")
  }

}

class Programmer extends Human {

  constructor(props) {
    super(props);

    this._validate(props);
    this.programmingLangs = props.programmingLangs;
  }

  _validate(props) {
    ["name", "address", "lang", "programmingLangs"].forEach(i => {
      if (!Object.keys(props).includes(i))
        throw new Error(`${i} is required!`);
    });
  }

  code() {
    console.log(`Writing some ${this.progammingLangs.sample()}`)
  }

  // Override Method
  work() {
    this.code();

    // Super is just this, but it is calling the super class
    super.work();
  }
}

const Ian = new Programmer({
  name: "Ian",
  address: "Malang",
  lang: "Jawa",
  programmingLangs: ["Javascript", "Go"]
})