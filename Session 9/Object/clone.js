const obj = {
  id: 1,
  name: "Fikri",
  address: "Solo"
} // Let's say we save it on Locker A 

const person = obj; // person => #01ASD01 owned by obj
// person only tells the runner that he's referencing the Locker A

// Change Person Property
person.name = "Another Person";
console.log('Obj:', obj);
/* The name of the object will change,
 * because of person still referencing
 * on Locker A
 * */

// How to clone
const clone = {...obj}; // Using spread operator
/*
 * Copy the content of Locker A,
 * Save it in another Locker,
 * Locker B
 * */

clone.name = "Rizky"; // Only change the data of the Locker B

console.log('Clone:', clone); // clone.name will change and won't affect the Locker A
console.log('Obj:', obj); // obj.name will remain the same

// Clone and modify directly
const another = { ...obj, name: "Fikri", isMarried: false }
console.log('Another:', another);
