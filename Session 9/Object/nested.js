let person = {
  name: "Fikri",

  // Nested Object
  address: {
    st: "Jl. Arjuna I",
    district: "Mojosongo",
    city: "Solo",

    // More nest
    province: {
      id: 1,
      name: "Jawa Tengah"
    },
  }
}

console.log(person.address.city) // Solo
console.log(person.address.province.id); // 1
