var id = 0;

function register(x, password, passwordConfirmation) {
  // Just a validation
  if (password !== passwordConfirmation) {
    throw new Error("Password doesn't match!");
  }

  return {
    id: ++id, 
    email,
    password
  }
}

const Fikri = register("test01@mail.com", "123456", "123456");
console.log(Fikri);

/*
 * Another Unary Example
 *
 * let a = 0;
 * console.log(a) // 0;
 * a++ // a = a + 1
 * console.log(a) // 1;
 * ++a // a = a + 1
 * console.log(a) // 2;
 * console.log(++a); // 3;
 * console.log(a++); // 3;
 * */
