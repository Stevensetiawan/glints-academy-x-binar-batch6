// Simple Object
const person = {
  name: "Adrian",
  address: "Jalan Poso, Jogja."
}

person.isMarried = true; // Using .propsName
person.gender = "Male";
person["pets"] = ["Cats", "Birds"] // Using [propsName]

// Dynamically create new props on an Object
person[person.gender == "Male" ? "wife" : "husband"] = {
  name: "Someone",
  gender: person.gender == "Male" ? "Female" : "Male"
}

/*
 * {
 *   name: "Adrian",
 *   address: "Jalan Poso, Jogja",
 *   isMarried: true,
 *   gender: "Male",
 *   wife: { // Nested Object
 *      name: "Someone",
 *      gender: "Female"
 *   }
 * }
 * */

function introduce() {
  console.log("Hi!");
}

let fikri = {
  introduce
}

fikri.introduce();

