let participants = [
  "Galih",
  "Fikri",
  "Adrian",
  "Timotius",
  "Rijal",
  "Titan",
  "Imam",
  "Nat",
  "Ian"
]; // Locker A

let persons = participants; // Persons still referencing Locker A
persons.pop(); // Removing some data in Locker A
console.log(participants); // Participants will also get affected
persons.push("Ian"); // Add data in Locker A

// Clone
let clone = [...participants]; // Save it to the Locker B
console.log(clone);
clone.pop()
console.log("ClonePopped:", clone);
console.log("Raw:", participants);

// Clone and Modify
let raw = [...persons] // Make raw
raw.splice(Math.floor(persons.length / 2), 1, "Hantu Tengah") // Adjust the array

// .splice
// .slice
// .push
// .pop
// .sort
// .filter
// .forEach
// .map

let another = ["Hantu Pertama", ...raw, "Hantu Terakhir"];
console.log("CloneAndModify:", another);
