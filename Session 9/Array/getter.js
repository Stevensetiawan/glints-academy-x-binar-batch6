let participants = [
  "Galih",
  "Fikri",
  "Adrian",
  "Timotius",
  "Rijal",
  "Titan",
  "Imam",
  "Nat",
  "Ian"
]; // Locker A 

// Length
let ln = participants.length;

// Index of an element
let index = participants.indexOf("Fikri");

// Is array included an element?
let doesFikriExist = participants.includes("Fikri");
console.log(doesFikriExist);
