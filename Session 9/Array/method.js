let participants = [
  "Galih",
  "Fikri",
  "Adrian",
  "Timotius",
  "Rijal",
  "Titan",
  "Imam",
  "Nat",
  "Ian"
]; // Locker A

/*
 * Conventionally, For Each
 *
 * for (let index = 0; index < participants.length; index++) {
 *   console.log(participants[index]);
 * }
 * */

// Built-In Javascript Method to do that!
participants.forEach(function(i, index) {
  // i = participants[index];
  console.log(`${index}: ${i}`);
})

// Sort -> BubbleSort etc.
let sortedArray = participants.sort(function(a, b) {
  // a is participants[n]
  // b is participants[n + 1]
  return a.charCodeAt(0) - b.charCodeAt(0);
}); // Locker B
console.log(sortedArray);

/*
 * Javascript Array .filter
 *
 * You need to explore about this
 * 1. .map
 * 2. .filter
 * 3. .reduce
 * 4. .slice
 * 5. etc
 *
 * */
