// Mutation
let participants = [
  "Galih",
  "Fikri",
  "Adrian",
  "Timotius",
  "Rijal",
  "Titan",
  "Imam",
  "Nat",
  "Ian"
]; // Locker A 

// Push and Pop
participants.push("Hantu Terakhir"); // Locker A
participants.pop(); // Locker A

// Shift and Unshift
participants.unshift("Hantu Pertama"); // Locker A
participants.shift(); // Locker A

// Splice
let midIndex = Math.floor(participants.length / 2);
participants.splice(midIndex, -1, "Hantu Tengah");
participants.splice(participants.indexOf("Hantu Tengah"), 1);
