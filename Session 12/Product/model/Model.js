'use strict'

const fs = require('fs');
// console.log(fs);

class Model {
    constructor(id, name, price, stock) {
        this.id = id
        this.name = name
        this.price = price
        this.stock = stock
    }

    static parseDate() {
        const read = fs.readFileSync('./Product.json', 'utf8')
        const parsed = JSON.parse(read)
        return parsed
    }

    static show() {
        const data = Model.parseDate()
        let arr = []
        data.forEach(element => {
            arr.push(new Model(element.id, element.name, element.price, element.stock))
        });
        // console.log(arr);
        return arr
    }

    static add(title, price, stock) {
        const data = Model.show()
        let newId = data[data.length - 1].id + 1

        let newData = new Model(newId, title, price, stock)

        data.push(newData)
        Model.save(data)

        return newData
    }

    static delete(id) {
        const data = Model.show()
        let arr = []
        let deleteId;
        for (let i = 0; i < data.length; i++) {
            if (data[i].id !== Number(id)) {
                arr.push(data[i])
            } else if (data[i].id === Number(id)) {
                deleteId = data[i]
            }
        }
        Model.save(arr)
        return deleteId
    }

    static findById(id) {
        const data = Model.show()
        for (let i = 0; i < data.length; i++) {
            if (data[i].id === Number(id)) {
                return data[i]
            }
        }
    }

    static update(id) {

    }

    static save(data) {
        fs.writeFileSync('./Product.json', JSON.stringify(data, null, 4))
        return `success`
    }


}

module.exports = Model

/*
Make two class who inherit Abstract Class called Record

Book,
    title
    author
    price
    publisher

Product,
    name,
    price,
    stock
*/