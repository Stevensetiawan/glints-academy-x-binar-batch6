'use strict'

const Model = require('../model/Model');
const View = require('../view/View');

class Controller {
    static help() {
        View.help()
    }

    static error() {
        View.error()
    }

    static show() {
        let data = Model.show()
        View.show(data)
    }

    static add(data) {
        let product = Model.add(data[0], Number(data[1]), Number(data[3]))
        View.message(console.log(`Successfully added new data Book ${product.name}`))
    }

    static delete(id) {
        let data = Model.delete(id)
        View.message(console.log(`Successfully deleted data Book ${data.name}`))
    }

    static findById(id) {
        let data = Model.findById(id)
        View.findById(data)
    }

    static update(id) {

    }

}

module.exports = Controller
