const command = process.argv[2]
// console.log(command);
const input = process.argv.slice(3)

const Controller = require('./controller/Controller');

switch (command) {
    case 'help':
        Controller.help()
        break;
    case 'show':
        Controller.show()
        break;
    case 'add':
        Controller.add(input)
        break;
    case 'delete':
        Controller.delete(input)
        break;
    case 'findById':
        Controller.findById(input)
        break;
    case 'update':
        Controller.update(input)
        break;
    default:
        Controller.error()
        break;
}
