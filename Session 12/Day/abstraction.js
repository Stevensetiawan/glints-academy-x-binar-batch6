/*
  Human =>
    Police work()
    Programmer work()
    Chef work()
    UnemployedPerson work()

  Abstraction is related to Abstract Class
  
  Abstract Class is a class where
  we can't instantiate an instance from it,
  unless it is instantiated from the sub class of that class.

  That means, everything that were defined inside the Abstract Class,
  were meant to be implemented by the sub class.

  So, abstract class is basically just a blueprint of another class
  */

class Human {

  constructor(props) {
    if (this.constructor === Human)
      throw new Error("Can't instantiate from Human");

    this.name = props.name;
    this.gender = props.gender;
  }

  introduce() {
    console.log(`Hi, my name is ${this.name}`)
  }

  work() {}
  marry() {}
}

class Police extends Human {

  constructor(props) {
    super(props);
  }

  // Override Method
  introduce() {
    super.introduce();
    console.log("And I'm a police");
  }

}

class Chef extends Human {
  
  constructor(props) {
    super(props);
    this.cuisines = props.cuisines;
    this.type = props.type;
  }
  
  introduce() {
    super.introduce();
    console.log(`And I'm a ${this.type} chef`)
  }

}

const Juna = new Chef({
  name: "Juna",
  gender: "Male",
  cuisines: ["Martabak"],
  type: "Generalist"
})

const Wiranto = new Police({
  name: "Wiranto",
  gender: "Male"
})

Wiranto.introduce();
Juna.introduce();