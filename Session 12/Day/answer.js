/*
  * Greet another Human
  * Marry another Human
  * Introduce themself
  * Learn new language
  * If person a tries to marry person b, make sure they've both understand the same language, otherwise, one of them will learn a new languange that both will understand.
*/

// To randomly call element inside the array
Object.defineProperty(Array.prototype, 'sample', {
  get: function() {
    return this[
      Math.floor(
        Math.random() * this.length
      )
    ]
  }
})

class Human {

  static properties = [
    "name",
    "gender",
    "langs"
  ]

  constructor(props) {
    this.validate(props)

    this.name = props.name;
    this.gender = props.gender;
    this.langs = props.langs;
  }

  // Because this is instance
  // We need to this.constructor to call the static props/method
  validate(props) {
    if (typeof props !== 'object' || Array.isArray(props))
      throw new Error("Constructor needs object to work with");

    this.constructor.properties.forEach(i => {
      if (!Object.keys(props).includes(i))
        throw new Error(`${i} is required`);
    });
  }

  greets(person) {
    console.log(`Hi, ${person.name}, I'm ${this.name}!`);
  }

  learn(language) {
    // Check if the language is already learned by the instance
    // If it is, then just ignore it!
    if (!this.langs.includes(language))
      this.langs.push(language);
  }

  marry(person) {
    let ready = false;

    // Check if both bride has the same language
    this.langs.forEach(i => {
      if (person.langs.includes(i)) ready = true;
    })

    // If not ready then learn new language
    if (!ready) this.learn(person.langs.sample);

    this.spouse = person;
    person.spouse= this;
  }

}

// Instantiate Human
let Fikri = new Human({
  name: "Fikri",
  gender: "Male",
  langs: ["Javanese", "Indonesia"]
})

let Woman = new Human({
  name: "Woman",
  gender: "Female",
  langs: ["English", "French"]
})

Fikri.greets(Woman);

Fikri.learn("Japanese");
Fikri.marry(Woman);