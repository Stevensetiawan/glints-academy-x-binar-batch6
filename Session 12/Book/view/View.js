'use strict'

class View {
    static message(message) {
        console.log(message);
    }

    static error() {
        throw new Error(`Invalid command! Please input a <command>, type "node app.js help" for more information`)
    }

    static help() {
        let command = [
            'node app.js help\n',
            'node app.js show\n',
            'node app.js add <title> <author> <price> <publisher>\n',
            'node app.js delete <task_id / id>\n',
            'node app.js findById <task_id / id>\n',
            'node app.js update\n' //masih belum bisa
        ]

        console.log(`\n+-------------------------------+`)
        console.log(`|  Listed Command for Book App  |`)
        console.log(`+-------------------------------+\n`)

        for (let i = 0; i < command.length; i++) {
            console.log(command[i]);
        }
    }

    static show(data) {
        console.table(data)
    }

    static findById(data) {
        console.log(`${data.id}. ${data.tittle}`)
    }
}

module.exports = View

/*
Make two class who inherit Abstract Class called Record

Book,
    title
    author
    price
    publisher

Product,
    name,
    price,
    stock
*/