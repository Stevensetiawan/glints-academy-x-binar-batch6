/*
  1. Create a class called Human
  2. Create sub class called Chef inherit from Human

  Human can cook();
  Human can introduce();

  Chef has cuisines;
  Chef type => Italian, French;
  Chef can promote();
  Chef can cook() but better // Method Override
  Chef will introduce themself as ${type} chef
  */
