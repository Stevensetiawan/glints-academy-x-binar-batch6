// To randomly call element inside the array
Object.defineProperty(Array.prototype, 'sample', {
  get: function() {
    return this[
      Math.floor(
        Math.random() * this.length
      )
    ]
  }
})

Array.prototype.random = function() {
  return this[
    Math.floor(
      Math.random() * this.length
    )
  ]
}

class Human {

  static properties = [
    "name",
    "gender",
    "languages"
  ]

  constructor(props) {
    // Validate the props
    this._validate(props);

    this.name = props.name;
    this.gender = props.gender;
    this.languages = props.languages;
  }

  // Validate, for input inside the constructor
  _validate(props) {
    if (typeof props !== 'object' || Array.isArray(props))
      throw new Error("Constructor needs object to work with");

    this.constructor.properties.forEach(i => {
      if (!Object.keys(props).includes(i))
        throw new Error(`${i} is required`);
    });
  }

  introduce() {
    console.log(`Hi, my name is ${this.name}`);
  }

  greets(person) {
    // Check if the person is a human
    if (!(person instanceof Human))
      throw new Error("You can only greet Human");
    
    console.log(`Hi, ${person.name}!`)
  }

  learnLanguage(language) {
    if (typeof language !== "string")
      throw new Error("Languange is must be a string");

    if (this.languages.includes(language)) return;

    this.languages.push(language);
  }

  marry(person) {
    // Check if ready;
    let ready = false;

    person.languages.forEach(i => {
      if (this.languages.includes(i))
        return ready = true;
    })

    

    if (!ready) {
      let fiancees = [this, person];
      let learner = fiancees.sample;
      let matchee = fiancees.indexOf(learner) == 1 ? fiancees[0] : fiancees[1];

      learner.learnLanguage(matchee.languages.sample);
    }

    this.spouse = person;
    person.spouse = this;
  }

}

class Programmer extends Human {

  static properties = [...super.properties, "programmingLanguages"]

  constructor(props) {
    super(props); // What is super?
    this.programmingLanguages = props.programmingLanguages;
  }

  code() {
    console.log(`${this.name} is writing ${this.programmingLanguages.sample} code`)
  }

  // Override Method
  introduce() {
    super.introduce();
    console.log("And I'm a programmer");
  }

  // Overload method
  learnLanguage({language, programmingLanguage}) {
    if (language) super.learnLanguage(language);
    if (programmingLanguage) this.programmingLanguages.push(programmingLanguage);
  }

}

const Fikri = new Programmer({
  name: "Fikri",
  gender: "Male",
  languages: ["Indonesia"],
  programmingLanguages: ["Javascript"]
})

Fikri.introduce();
Fikri.learnLanguage({
  language: "Javanese",
  programmingLanguage: "Ruby"
})

console.log(Fikri);