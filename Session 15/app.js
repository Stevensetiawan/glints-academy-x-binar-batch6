const express = require('express');
const app = express()
const cors = require('cors')
const PORT = process.env.PORT || 3000


// Middleware
const router = require('./router.js');

app.use(cors())

// Middleware to Parse JSON
app.use(express.json()); // Coba sendiri.

app.get('/', function (req, res) {
  res.json({
    status: true,
    message: "Hello World!"
  })
})
app.use('/', router);
 
http.listen(PORT, function () {
  console.log(`listening on ${PORT}`);
});