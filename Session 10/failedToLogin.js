const { EventEmitter } = require('events');
const panitia = new EventEmitter();

const users = {};

panitia.on("Unauthorized", payload => {
  let { id, email } = payload;

  const data = users[id.toString()];

  if (data) {

    // Notify User
    if (data.loginCount >= 5) {
      return console.log(`Notify ${data.email}`)
    }

    users[id.toString()] = {
      email,
      loginCount: ++data.loginCount
    }
  } else {
    users[id.toString()] = {
      email,
      loginCount: 1
    }
  }
})

panitia.emit("Unauthorized", {
  id: 1,
  email: 'test01@mail.com',
})

panitia.emit("Unauthorized", {
  id: 1,
  email: 'test01@mail.com',
})

panitia.emit("Unauthorized", {
  id: 1,
  email: 'test01@mail.com',
})

panitia.emit("Unauthorized", {
  id: 1,
  email: 'test01@mail.com',
})

panitia.emit("Unauthorized", {
  id: 1,
  email: 'test01@mail.com',
})

panitia.emit("Unauthorized", {
  id: 1,
  email: 'test01@mail.com',
})

console.log(users);