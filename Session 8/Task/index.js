/*
 * DON'T CHANGE
 * */

const data = [];
const randomNumber = Math.floor(Math.random() * 100);

function createArray() {
  for (let i = 0; i < randomNumber; i++) {
    data.push(createArrayElement())
  }

  // Recursive
  if (data.length == 0) {
    createArray();
  }
}

function createArrayElement() {
  let random = Math.floor(Math.random() * 1000);

  return [null, random][Math.floor(Math.random() * 2)]
}

createArray()

/*
 * Code Here!
 * */
function clean(data) {
  let result = new Array();

  // Check every single element
  for (let i = 0; i < data.length; i++) {
    if(data[i] !== null)
      result.push(data[i]);
  }

  return result;
}

/*
 * DON'T CHANGE
 * */

if (process.argv.slice(2)[0] == "test") {
  try {
    console.log("Raw:", data)
    let result = clean(data);

    console.log("Result:", result);
    result.forEach(i => {
      if (i == null) {
        throw new Error("Array still contains null")
      }
    })

    console.log("Work:", true)
  }

  catch(err) {
    console.error(err.message);
    console.log("Work:", false)
  }
}