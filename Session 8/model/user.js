const readline = require('../lib/readline.js');
const fs = require('fs');
const db = require('../db/user.json');

function register(email, password) {
  if (find(email)) {
    console.log("Email already exist!")
    return readline.close()
  }

  let id = db.length + 1;

  // Push new user into database
  db.push({
    id,
    email,
    password
  })

  fs.writeFileSync(`${__dirname}/../db/user.json`, JSON.stringify(db, null, 2));
  readline.close();
}

function login(email, password) {
  let user = find(email);
  if (!user) {
    console.log("Email doesn't exist")
    return readline.close()
  }

  if (user.password !== password) {
    console.log("Wrong password!")
    return readline.close()
  }

  console.log(`Hello, ${email}`);
  console.log('Token:', generateToken(user.id));

  // Do something
  readline.close();
}

function find(email) {
  // Check every single data, if there's already been exist then
  for (let i = 0; i < db.length; i++) {
    if (db[i].email == email) return db[i];
    // That means there's already been the same email exist in DB.
  }

  // If this code run, that means there's no email in the DB. 
  return null;
}

function generateToken(id) {
  let char = ['A', 'B', 'C', 'D', 'E'];
  return char[Math.floor(Math.random() * char.length)] + id;
}


module.exports = {
  register,
  login
}
