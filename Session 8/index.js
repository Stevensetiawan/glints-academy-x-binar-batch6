/*
 * Registry,
 * Handle user registration and save it to a file.
 * 1. Ask User for Input
 * 2. Validate Input
 * 3. Check Database
 * 4. Save the Data
 * */

/* Email Regex */
/* Feature Login */

const fs = require('fs');

const readline = require('./lib/readline.js');
const user = require('./model/user.js');

function validateEmail(email) {
  const regex =  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return regex.test(email)
}

function askForRegister() {
  console.clear(); // Clear up console
  console.log("Facebook Registration")
  readline.question("Email: ", email => {
    if (!validateEmail(email)) {
      console.error(`${email} is not a valid email!`);
      return readline.close();
    }

    readline.question("Password: ", password => {
      user.register(email.toLowerCase(), password)
    })
  })
}

function askForLogin() {
  console.clear()
  console.log("Facebook Login")
  readline.question("Email: ", email => {
    readline.question("Password: ", password => {
      user.login(email.toLowerCase(), password);
    })
  })
}

console.log("What do you want to do?")
console.log(`
1. Login
2. Register
3. Exit
`)
readline.question("Answer: ", answer => {
  if (answer == 1) return askForLogin();
  if (answer == 2) return askForRegister();
  if (answer == 3) return readline.close();

  console.log("Wrong option!");
  readline.close();
})
