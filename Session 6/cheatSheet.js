function randomNumber() {
  let random = Math.floor(Math.random() * 100);

  if (!random) {
    return randomNumber()
  }

  return random;
}

function createArray() {
  let arr = []
  for (let i = 0; i < randomNumber(); i++) {
    arr.push(createArrayElement())
  }

  return arr;
}

function createArrayElement() {
  return Math.floor(Math.random() * 1000);
}

const arr = createArray();

console.log('Raw:', arr);

// Filter Method
let result = arr.filter(i => i > 500);
console.log('Filtered:', result);

// Map Method
result = arr.map(i => i * 2);
console.log('Mapped:', result);

// forEach Method
// arr.forEach(i => console.log(i))

// sort Method
result = arr.sort((a, b) => a - b);
console.log('ASC:', result);

result = arr.sort((a, b) => b - a);
console.log('DSC:', result);
