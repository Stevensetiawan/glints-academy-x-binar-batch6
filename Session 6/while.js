/*
 * while loop
 * */

let i = 0; // Global Variable
while(i < 10) {
  console.log("Before:", i)

  i++; // Statement to make the loop going to break

  console.log("After:", i)
}

let arr = [8,9,10,5,2,0,6,10];

i = 0;
while (i < arr.length) {
  console.log(arr[i] * 2);
  i++;
}
