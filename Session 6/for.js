/*
 * for loop
 * */

for (i = 0; i < 10; i += 2) {
  console.log(i) // Always increment
  console.log("Ini putaran ke", i + 1);
}

let arr = [8,9,10,5,2,0,6,10];

for (let i = 0; i < arr.length; i++) {
  let elemen = arr[i];
  let result = elemen * 2;
  console.log(result);
}
