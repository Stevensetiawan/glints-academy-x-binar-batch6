'use strict';
module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define('user', {
    email: DataTypes.STRING,
    encrypted_password: DataTypes.STRING
  }, {});
  user.associate = function(models) {
    // associations can be defined here
  };

  user.register = function({ email, password }) {
    const salt = bcrypt.genSaltSync(10);
    const hash = bcrypt.hashSync(password, salt);

    return this.create({
      email,
      encrypted_password: hash
    })
  }

  return user;
};
