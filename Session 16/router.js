const express = require('express');
const router = express.Router();
const product = require('./controllers/productController');

/* Product API Collection */
router.get('/products', product.all);
router.get('/products/available', product.available);
router.get('/products/:id', product.available);

/* User API Collection */

module.exports = router;