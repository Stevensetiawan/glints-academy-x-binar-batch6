const { user: User } = require('../models');
const bcrypt = require('bcryptjs');

module.exports = {

  register(req, res) {
    /*
      1. Data from User
      2. Change plain password into encrypted password using bcrypt
      3. Then save the result to the users table.
    */
    const salt = bcrypt.genSaltSync(10);
    const hash = bcrypt.hashSync(req.body.password, salt);

    User.create({
      email: req.body.email,
      encrypted_password: hash
    })
      .then(user => {
        res.status(200).json({
          status: "success",
          data: {
            user
          }
        })
      })
      .catch(err => {
        res.status(422).json({
          status: "fail",
          errors: [err.message]
        })
      })
  },

  registerWithModel(req, res) {
    User.register(req.body)
      .then(user => {
        res.status(200).json({
          status: "success",
          data: {
            user
          }
        })
      })
      .catch(err => {
        res.status(422).json({
          status: "fail",
          errors: [err.message]
        })
      })
  },

  login(req, res) {
    /*

      1. Find user by email
      2. Get the encrypted_password property
      3. Compare with req.body.password

      */
     User.findOne(req.body.email)
     .then(user => {
         if (user){
            if(bcrypt.compareSync(req.body.password, hash)){
                res.status(200).json({
                    status: "success",
                    data: {
                        user
                    }
                })
            } else {
                return 0
            }                    
        } else {
            return 0
        }
     })
     .catch(err => {
         res.status(500).json({
             status: "login fail",
             errors: [err.message]
         })
     })
  }

}
